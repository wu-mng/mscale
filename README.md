# mscale
is a modular scale calculator, suitable for setting typography and spacing while designing websites or apps.
### usage
In the most simple scenario, just set the **scale** ratio you want to use.<br>

example: ```$scale: $golden-ratio;```<br>

You can choose from the most common scales, listed at the beginning of ```mscale.scss```, or really put any number that makes sense. Then compile the scss file (you can check ```style.css``` as an example of what will come out), and your typography is served. 

You can also use the automatically generated **classes** for paddding and margins:<br>
- ```.p-1```-```.p-5```  for ```padding:```
- ```.pt-1```-```.pt-5``` for ```padding-top:```
- ```.py-1```-```.py-5``` for ```padding-top:``` ```padding-bottom:```
- ```.py-x```-```.px-5``` for ```padding-left:``` ```padding-right:```
and so on...<br> 
Same thing for ```margin```, just use ```.m-1```-```.m-5```.

This way you'll be able to space your elements using the values of the scale, thus keeping a consistent vertical rhythm.

### optional values
```$mobile-font: 16px;```

This is the "base" font, it will influence the value of ```html {font-size: x% }```, which is the base for the whole scale.
Note that this option **must be set in px**.

```
$laptop-font: 18px;
$desktop-font: 20px;
```
Here you can set different values if you want the base font to look bigger on screens larger than 992px and 1600px; otherwise just copy the value of ```$mobile-font```. Value **must be in px**.
```
$body-line-height: $scale;
$headings-line-height: 1;  
```
The value of ```line-height``` for ```body``` (and ```p```) is based on the ratio of ```$scale```, with a fallback to ```1.4``` in case the scale is smaller than that. You can change the value for headings, if needed.

```
// Characters per line //
$cls: 21;
// Characters per line for landscape phones (screen > 576px) //
$clm: 34;
// Characters per line for tablets and up (screen > 768px) //
$cll: 55;
```
Set the desired characters per line, space excluded. The values here are arbitrary, although based on the most commonly used ones, and on Fibonacci's sequence(!). These options will determine the ```max-width``` of the ```<article>``` element.

### how it works
As we said, the values set in ```$mobile-font```, ```$laptop-font``` and ```$desktop-font``` will determine the ```font-size:``` of the ```html``` element.
```
// calculate the html font-size in percentage //
@function rootFont($x) {
    @return ($x / 16px) * 100%;
}
```
```
html { font-size: rootFont($mobile-font); }

(...)

@media (min-width: 992px) { 
    html { font-size: rootFont($laptop-font); }
}

(...)

@media (min-width: 1600px) {
    html { font-size: rootFont($desktop-font); }
}
```
Here we calculate the scale:
```
// Scale Generator //
$base-scale-font: 1rem;
$scale-step: 1rem;
@for $i from 1 to 6 {
  $step: round($base-scale-font * $scale, 3);
  $base-scale-font: $step;
  $scale-step: append($scale-step, $step);
}
```
basically we create an array (```$scale-step```) where we append the increasing values of the scale.<br>
Then we can apply them to
```
body { font-size: nth($scale-step, 1); }
```
and
```
h1 { font-size: nth($scale-step, 6); }
h2 { font-size: nth($scale-step, 5); }
h3 { font-size: nth($scale-step, 4); }
h4 { font-size: nth($scale-step, 3); }
h5 { font-size: nth($scale-step, 2); }
h6 { font-size: nth($scale-step, 1); }
```
The "first step" of the scale, and thus the ```font-size``` of ```body``` will always be ```1rem```, calculated on ```html { font-size: x% }```.

Let's have a look also at how the classes for ```padding``` and ```margin``` are generated:
```
$sides: (all, y, x, top, right, bottom, left);
@each $side in $sides {
    $i: 1;
    @for $i from 1 through 5 {
        $val: nth($scale-step, $i);
        @if $side == "all" {
            .p-#{$i} { padding: $val; }
            .m-#{$i} { margin: $val; }
        } 
        @else if $side == "y" {
            .p#{$side}-#{$i} { 
                padding-top: $val; 
                padding-bottom: $val;
            }
            .m#{$side}-#{$i} { 
                margin-top: $val; 
                margin-bottom: $val;
            }
        } 
        @else if $side == "x" {
            .p#{$side}-#{$i} { 
                padding-left: $val; 
                padding-right: $val;
            }
            .m#{$side}-#{$i} { 
                margin-left: $val; 
                margin-right: $val;
            }                        
        } 
        @else {
            $short: (str-slice($side, 1, 1));
            .p#{$short}-#{$i} { padding-#{$side}: $val; }
            .m#{$short}-#{$i} { margin-#{$side}: $val; }
        }
        $i: $i +1;
    }
}
```
The key here is ```$val: nth($scale-step, $i);```<br>
We cicle from 1 through 5 and we apply the corresponding position of the ```$scale-step``` array (you can increase the number if you want, but in any case it shouldn't be bigger than the ```$scale-step``` array), then we apply the values to each one of the ```$sides: (all, y, x, top, right, bottom, left);```

